The main goals of this project are: 

* Prove the value of distribute block chain approaches that can be applied to logistic use-cases (provided by industry partners and [compartment 1](https://www.dl4ld.net) collaborators)
* Provide a future proof cryptographic approach considering the processing power quantum computing and other high performance compute technologies may bring.
* Provide an environment allowing parties to create data sharing agreements compliant to the digital market place policy thus guaranteeing secure and trustworthy real time sharing of big data.
* Provide secure mechanisms for compliancy checking using provenance trails and audit trails, and mechanisms for dispute settlement in case of breaches. 
