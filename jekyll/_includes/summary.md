This proposal concerns multidisciplinary fundamental and applied research on developing a secure, end-to-end trusted, scalable and future-proof solution for distributed dynamic data sharing across multiple logistic domains. 
Sharing data across organizations in a secure, trusted and policy based manner, and enforcing the agreed use of that data is a hard problem. 
Many partial solutions already exist in the form of platforms, standards and directives, and back office solutions, all of them aimed at sharing data with their own goals and objectives.
The absence of integrated solutions is hindering the potential exploitation of fused big data.
Indeed, the research line "Data security in logistics" in the call underlines the importance of integrated cryptographically secure solutions.

In our proposal, typical logistics application profiles on specific demands in security, privacy, performance and policy will be identified. The goal of the proposed research is to develop end-to-end trusted and scalable solutions for typical application profiles that allow policy enforced data sharing and analysis across multiple IT domains based on negotiation of smart multi-party contracts. 
To achieve our goals, we will develop and integrate techniques for secure data sharing, scalability for big data, and policy-based data sharing and analytics.
The ability to form temporary business alliances relies on the ability to implement ways to keep track of business agreements and transaction information in ways trusted by community members. 
The agility and diversity of market players, its data sources and potential risks if data is misused, is helped by creating a commonly trusted and governed infrastructure to access and share data. 
Such type of infrastructure is subject of research in Compartiment 1 of this call. 
In this proposal we will work alongside this work in order to investigate the value that blockchain technology will bring tosharing big data assets in a big data sharing infrastructure. The ability to provided trusted information from many distributed sources, such as mobile of tablet style devices that to not have to computational power to participate in traditional blockchain approaches, is an important topic address by this work.
The aim is to not only protect primary data and limit accessibility via policy, but also to secure access to the provenance trails and other meta-data that are sensitive.
